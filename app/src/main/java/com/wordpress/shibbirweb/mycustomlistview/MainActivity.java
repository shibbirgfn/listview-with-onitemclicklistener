package com.wordpress.shibbirweb.mycustomlistview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView playerListLV;

    PlayerAdapter playerAdapter;

    List<ModelPlayer> list = new ArrayList<ModelPlayer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        playerListLV = (ListView) findViewById(R.id.lv_activity_main_playerList);

        list.add(new ModelPlayer(R.drawable.masrafi,"Mashrafe Mortaza","34","October 5, 1983","Norail","Bowler","Mashrafe Bin Mortaza is a Bangladesh international cricketer, and current captain of the One Day Internationals for Bangladesh national cricket team. He is also a former T20I captain, until his retirement."));

        list.add(new ModelPlayer(R.drawable.tamim, "Tamim Iqbal","29","March 20, 1989","Chattagram","Openar Batsman","Muhammad Tamim Iqbal Khan is a Bangladeshi international cricketer and former Test captain of the Bangladeshi national team. Tamim made his One Day International debut in 2007 and played his first Test the following year."));

        list.add(new ModelPlayer(R.drawable.sakib,"Shakib Al Hasan","31","March 24, 1987","Magura","All rounder","Shakib Al Hasan is a Bangladeshi international cricketer who currently captains the Bangladesh national team in test and T20I formats."));

        list.add(new ModelPlayer(R.drawable.musfiq,"Mushfiqur Rahim","31","June 9, 1987","Bogura","Wicket Keeper ","Mohammad Mushfiqur Rahim is a Bangladeshi cricketer and the former captain of the Bangladesh national cricket team. Between August 2009 and December 2010 Rahim served as Bangladesh's vice-captain in all formats."));

        list.add(new ModelPlayer(R.drawable.sabbir,"Sabbir Rahman","26","November 22, 1991","Bogura","Batsman ","Sabbir Rahman is a Bangladeshi cricketer. He is from Rajshahi and besides national team he also plays for Rajshahi Division. He is an All-Rounder. Sabbir is a right-handed batsman and legbreak bowler."));

        playerAdapter = new PlayerAdapter(getApplicationContext(), list);
        playerListLV.setAdapter(playerAdapter);

        playerListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),DetailsActivity.class);
                intent.putExtra("position",position);
                intent.putExtra("playerList", (Serializable) list);
                startActivity(intent);
            }
        });


    }
}
