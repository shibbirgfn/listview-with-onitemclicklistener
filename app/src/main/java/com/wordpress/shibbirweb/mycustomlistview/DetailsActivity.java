package com.wordpress.shibbirweb.mycustomlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class DetailsActivity extends AppCompatActivity {

    ImageView playerImage;
    TextView playerName, playerAge, playerBorn, playerPosition, playerBio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        playerImage = (ImageView) findViewById(R.id.iv_activity_details_player_image);
        playerName = (TextView) findViewById(R.id.tv_activity_details_player_name);
        playerAge = (TextView) findViewById(R.id.tv_activity_details_player_age);
        playerBorn = (TextView) findViewById(R.id.tv_activity_details_player_born_date);
        playerPosition = (TextView) findViewById(R.id.tv_activity_details_player_position);
        playerBio = (TextView) findViewById(R.id.tv_activity_details_player_bio);


        int position = getIntent().getIntExtra("position",0);
        List<ModelPlayer> playerList = (ArrayList<ModelPlayer>) getIntent().getSerializableExtra("playerList");


        playerImage.setImageResource(playerList.get(position).getPlayerImage());
        playerName.setText(playerList.get(position).getPlayerName());
        playerAge.setText(playerList.get(position).getPlayerAge());
        playerBorn.setText(playerList.get(position).getPlayerBirthDate());
        playerPosition.setText(playerList.get(position).getPlayerPosition());
        playerBio.setText(playerList.get(position).getPlayerBio());
    }
}
