package com.wordpress.shibbirweb.mycustomlistview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PlayerAdapter extends ArrayAdapter {

    List<ModelPlayer> modelPlayers = new ArrayList<ModelPlayer>();

    public PlayerAdapter(@NonNull Context context, List<ModelPlayer> modelPlayers) {
        super(context, R.layout.item, modelPlayers);
        this.modelPlayers = modelPlayers;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item,parent,false);

        ImageView playerImage = view.findViewById(R.id.iv_item_player_image);
        TextView playerName = view.findViewById(R.id.tv_item_player_name);
        TextView playerPosition = view.findViewById(R.id.tv_item_player_position);

        playerImage.setImageResource(modelPlayers.get(position).getPlayerImage());
        playerName.setText(modelPlayers.get(position).getPlayerName());
        playerPosition.setText(modelPlayers.get(position).getPlayerPosition());

        return view;

    }
}
