package com.wordpress.shibbirweb.mycustomlistview;


import java.io.Serializable;

public class ModelPlayer implements Serializable{
    private int playerImage;
    private String playerName, playerAge, playerBirthDate, playerHometown, playerPosition, playerBio;

    public ModelPlayer() {
    }

    public ModelPlayer(int playerImage, String playerName, String playerAge, String playerBirthDate, String playerHometown, String playerPosition, String playerBio) {
        this.playerImage = playerImage;
        this.playerName = playerName;
        this.playerAge = playerAge;
        this.playerBirthDate = playerBirthDate;
        this.playerHometown = playerHometown;
        this.playerPosition = playerPosition;
        this.playerBio = playerBio;
    }

    public int getPlayerImage() {
        return playerImage;
    }

    public void setPlayerImage(int playerImage) {
        this.playerImage = playerImage;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerAge() {
        return playerAge;
    }

    public void setPlayerAge(String playerAge) {
        this.playerAge = playerAge;
    }

    public String getPlayerBirthDate() {
        return playerBirthDate;
    }

    public void setPlayerBirthDate(String playerBirthDate) {
        this.playerBirthDate = playerBirthDate;
    }

    public String getPlayerHometown() {
        return playerHometown;
    }

    public void setPlayerHometown(String playerHometown) {
        this.playerHometown = playerHometown;
    }

    public String getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(String playerPosition) {
        this.playerPosition = playerPosition;
    }

    public String getPlayerBio() {
        return playerBio;
    }

    public void setPlayerBio(String playerBio) {
        this.playerBio = playerBio;
    }
}
